import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const AuthServiceStamp = stampit.methods({
  login({ payload }) {
    return this.fetchApi.post(`login`, payload);
  },
  logout({ payload }) {
    return this.fetchApi.post(`logout`, payload);
  },
  resetPassword({ payload }) {
    return this.fetchApi.put(`credentials/password`, payload)
  },
  forgotPassword({ payload }) {
    return this.fetchApi.post(`credentials/password`, payload)
  }
});

const AuthService = stampit.compose(ServiceStamp, AuthServiceStamp);
export default AuthService;
