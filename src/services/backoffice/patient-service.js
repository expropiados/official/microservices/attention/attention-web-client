import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const PatientServiceStamp = stampit.methods({
  getAllPatients({specialistUuid,name}){
    if (name==''){
      return this.fetchApi.get(`specialists/${specialistUuid}`);
    }else{
      return this.fetchApi.get(`specialists/${specialistUuid}?search=${name}`);
    }
  }
});

const PatientService = stampit.compose(ServiceStamp, PatientServiceStamp);
export default PatientService;
