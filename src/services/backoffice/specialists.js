import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const SpecialistsStamp = stampit.methods({
  getSpecialists() {
    return this.fetchApi.get('specialists');
  },
  getInfoExtraSpecialist({ specialistUuid }) {
    return this.fetchApi.get(`v2/specialists/${specialistUuid}/extra-info`);
  },
  getInfoSpecialist({ specialistUuid }) {
    return this.fetchApi.get(`specialists/${specialistUuid}`);
  },
});

const SpecialistsService = stampit.compose(ServiceStamp, SpecialistsStamp);
export default SpecialistsService;
