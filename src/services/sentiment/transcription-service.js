import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const TranscriptionServiceStamp = stampit.methods({
  getTranscription({ sessionId }) {
    let response = this.fetchApi.get(`transcriptions/${sessionId}`);
    console.log("responsee", response);
    return response;
  },
  getAnalysis({ transcriptionId, videocallUuid }) {
    return this.fetchApi.post(`getAnalysis?transcription_id=${transcriptionId}&vdeocall_uuid=${videocallUuid}`);
  }
});

const TranscriptionService = stampit.compose(ServiceStamp, TranscriptionServiceStamp);
export default TranscriptionService;
