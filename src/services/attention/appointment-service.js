import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const AppointmentServiceStamp = stampit.methods({
  getAppointment({ id }) {
    return this.fetchApi.get(`appointments/${id}/appointment-detail`);
  },
  getAllAppointmentsByDate({ specialistUuid, dateIni, dateEnd }) {
    return this.fetchApi.get(`specialists/${specialistUuid}/appointments?dateBegin=${dateIni}&dateEnd=${dateEnd}`);
  },
  getAllAppointmentsBySpecialistPatient({ specialistUuid, patientUuid }) {
    return this.fetchApi.get(`specialists/${specialistUuid}/patientSessions?uuidPatient=${patientUuid}`);
  },
  registerLinkInDetailAppointment({ idAppointment, payload }) {
    const json = JSON.stringify(payload, null, ' ');
    return this.fetchApi.post(`appointments/appointment-detail/${idAppointment}/resource/link`, payload);
  },
  registerDocumentInDetailAppointment({ idAppointment, payload }) {
    // const json = JSON.stringify(payload, null, ' ');
    return this.fetchApi.post(`appointments/appointment-detail/${idAppointment}/resource/document`, payload,
      { headers: { 'Content-Type': 'multipart/form-data' } });
  },
  getAppointmentDetail({ idAppointment }) {
    return this.fetchApi.get(`appointments/${idAppointment}/appointment-detail`);
  },
  getListAppointmentsByPatient({ patientUuid, dateIni, dateEnd }) {
    return this.fetchApi.get(`patients/${patientUuid}/appointments?dateBegin=${dateIni}&dateEnd=${dateEnd}`);
  },
  saveResults({ sessionId, results }) {
    const json = {
      results,
    };
    const resultado = new String(results);
    console.log('resultado', results);
    return this.fetchApi.post(`appointments/${sessionId}/results`, resultado);
  },
  cancelAppointment({ idAppointment }) {
    return this.fetchApi.delete(`appointments/${idAppointment}/cancel`);
  },
  getReasonsPreAppointment() {
    return this.fetchApi.get('appointments/medical-reasons');
  },
  registerPrePolls({ idAppointment, payload }) {
    return this.fetchApi.post(`appointments/${idAppointment}/preAppointmentPool`, payload);
  },
});

const AppointmentService = stampit.compose(ServiceStamp, AppointmentServiceStamp);
export default AppointmentService;
