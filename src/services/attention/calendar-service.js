import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const AvailabilityServiceStamp = stampit.methods({
  registerAvailability({ specialistUuid, payload }) {
    const json = JSON.stringify(payload, null, ' ');
    return this.fetchApi.post(`specialists/${specialistUuid}/availability`, payload);
  },
  getAvailability({ specialistUuid, dateBegin, dateEnd }) {
    return this.fetchApi.get(`specialists/${specialistUuid}/availabilityRangeSpecialist?dateBegin=${dateBegin}&dateEnd=${dateEnd}`);
  },
});

const AvailabilityService = stampit.compose(ServiceStamp, AvailabilityServiceStamp);
export default AvailabilityService;
