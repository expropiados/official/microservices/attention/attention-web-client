import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const VideoCallServiceStamp = stampit.methods({
  registerVideoCall(videocall) {
    return this.fetchApi.post('video_call', videocall);
  },
  getAgoraName(id) {
    return this.fetchApi.get(`/appointments/${id}/video-call/agora-channel`);
  },
});

const VideoCallService = stampit.compose(ServiceStamp, VideoCallServiceStamp);
export default VideoCallService;
