import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const PatientServiceStamp = stampit.methods({
  getPatient({ id }) {
    return this.fetchApi.get(`attention/patients/${id}`);
  },
  registerAppointment(appointment) {
    return this.fetchApi.post('appointments', appointment);
  },
  sendAnswers({ sessionId, answers }) {
    const json = {
      "callRating": answers.callRating,
      "selectedAudioIssues":  answers.selectedAudioIssues,
      "selectedVideoIssues":  answers.selectedVideoIssues,
      "callComments":  answers.callComments,
      "doctorRating":  answers.doctorRating,
      "selectedDoctorIssues":  answers.selectedDoctorIssues,
      "doctorComments":  answers.doctorComments,
    };
    console.log("session id", sessionId);
    console.log('respuestas json: ', json);
    return this.fetchApi.post(`appointments/${sessionId}/post-poll`, json);
  },
});

const CompanyService = stampit.compose(ServiceStamp, PatientServiceStamp);
export default CompanyService;
