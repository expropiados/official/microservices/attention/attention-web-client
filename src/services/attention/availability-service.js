import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const AvailabilityServiceStamp = stampit.methods({
  getSpecialistAvailability({ id }) {
    return this.fetchApi.get(`attention/availabilities/specialist/${id}`);
  },
});

const AvailabilityService = stampit.compose(ServiceStamp, AvailabilityServiceStamp);
export default AvailabilityService;
