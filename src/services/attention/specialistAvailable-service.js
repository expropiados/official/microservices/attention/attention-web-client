import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const SpecialistAvailableServiceStamp = stampit.methods({
  getAllSpecialistAvailableByDate({ date, uuidSpecialist }) {
    return this.fetchApi.get(`specialists/availability?date=${date}&specialistUuid=${uuidSpecialist}`);
  },
  getAllSpecialistAvailableByDateRange({ startDate, endDate }) {
    return this.fetchApi.get(`specialists/availability?dateBegin=${startDate}&dateEnd=${endDate}`);
  },
});

const SpecialistAvailableService = stampit.compose(ServiceStamp, SpecialistAvailableServiceStamp);
export default SpecialistAvailableService;
