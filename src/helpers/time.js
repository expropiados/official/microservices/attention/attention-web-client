function formatDateN(d) {
  let ye = new Intl.DateTimeFormat("en", { year: "numeric" }).format(d);
  let mo = new Intl.DateTimeFormat("en", { month: "2-digit" }).format(d);
  let da = new Intl.DateTimeFormat("en", { day: "2-digit" }).format(d);
  return `${ye}-${mo}-${da}`;
}

export const formatAMPM = (date) => {
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'pm' : 'am';
  hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  hours = hours < 10 ? '0'+hours : hours;
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}

export const getWeek = (value) => {
  let curr = new Date();
  if (value) {
    curr = new Date(value);
    curr.setHours(curr.getHours() + 5);
  }
  let first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
  let last = new Date(first); // last day is the first day + 6
  last = last.getDate() + 6
  let dateBegin = new Date(curr.setDate(first));
  let dateEnd = new Date(curr.setDate(last));
  dateBegin = formatDateN(dateBegin);
  dateEnd = formatDateN(dateEnd);
  return { dateBegin, dateEnd };
}
