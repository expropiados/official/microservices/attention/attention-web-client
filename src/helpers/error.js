export const getError = (error) => {
  let message = 'Error interno'
  if (error.response) {
    console.log(error.response.data)
    const { code, data } = error.response.data
    switch (code) {
      case 'SEC_LOG_003': {
        message = `Contraseña incorrecta para el usuario ${data.email}`;
        break;
      }
      case 'ATT_APP_001': {
        message = `Cita no encontrada`;
        break;
      }
      default: {
        message = 'Error con código desconocido';
      }
    }
  } else if (error.request) {
    message = 'Error de conexion';
  } else {
    message = 'Error interno';
  }
  return message;
};
