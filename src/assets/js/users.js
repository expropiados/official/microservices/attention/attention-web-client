const userTypes = {
  patient: {
    id: 0,
    name: 'pa_id',
  },
  specialist: {
    id: 1,
    name: 'sp_id',
  },
  client_supervisor: {
    id: 2,
    name: 'cs_id',
  },
  prov_supervidor: {
    id: 3,
    name: 'ps_id',
  },
  admin: {
    id: 4,
    name: 'ad_id',
  },
};

export default userTypes;
