import userTypes from "./users"
import images from "./images"

const rolesinfo = [
  {
    image: images.iconAdmin,
    title: "Administrador",
    id: userTypes.admin.id,
    route: '/',
    options: [{ to: "" }]
  },
  {
    image: images.iconSProv,
    title: "Supervisor Proveedor",
    id: userTypes.prov_supervidor.id,
    route: '/especialistas',
    options: [{ to: "" }]
  },
  {
    image: images.iconSClient,
    title: "Supervisor Cliente",
    id: userTypes.client_supervisor.id,
    route: '/pacientes',
    options: [{ to: "" }]
  },
  {
    image: images.iconSpecialist,
    title: "Especialista",
    id: userTypes.specialist.id,
    route: "/especialista/horario",
    options: [
      {
        title: "Mi horario",
        icon: "mdi-calendar",
        to: "/especialista/horario"
      },
      {
        title: "Mis citas",
        icon: "mdi-clock-time-seven-outline",
        to: "/especialista/cita"
      },
      {
        title: "Mis pacientes",
        icon: 'mdi-account-multiple',
        to: "/especialista/pacientes"
      },
    ]
  },
  {
    image: images.iconPatient,
    title: "Paciente",
    id: userTypes.patient.id,
    route: "/paciente/registro-cita",
    options: [
      {
        title: "Registrar cita",
        icon: "mdi-calendar",
        to: "/paciente/registro-cita"
      },
      {
        title: "Mis citas",
        icon: "mdi-clock-time-seven-outline",
        to: "/paciente/sesion"
      },
    ]
  }
]

export default rolesinfo
