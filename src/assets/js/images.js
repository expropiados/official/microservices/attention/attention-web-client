const images = {
  login: 'https://crowdonation-resources.s3.amazonaws.com/bglogin2.jpg',
  iconAdmin: 'https://crowdonation-resources.s3.amazonaws.com/Icono_administrador.png',
  iconSpecialist: 'https://crowdonation-resources.s3.amazonaws.com/Icono_especialista.png',
  iconPatient: 'https://crowdonation-resources.s3.amazonaws.com/Icono_paciente.png',
  iconSClient: 'https://crowdonation-resources.s3.amazonaws.com/Icono_supervisor_cliente.png',
  iconSProv: 'https://crowdonation-resources.s3.amazonaws.com/Icono_supervisor_proveedora.png' ,
  whitebg: 'https://crowdonation-resources.s3.amazonaws.com/whitebg.jpg'
};

export default images;
