import Vue from 'vue';
import { getError } from '~/helpers/error';

export const state = () => ({
  content: '',
  color: ''
})

export const mutations = {
  showMessage(_state, { content, color }) {
    if (color === 'error') {
      _state.content = getError(content);
    } else {
      _state.content = content;
    }
    _state.color = color
  }
}
