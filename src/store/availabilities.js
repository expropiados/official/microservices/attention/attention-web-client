import Vue from 'vue';
import AvailabilityService from '@/services/attention/availability-service';

export const state = () => ({
  availability: {},
  availabilities: {},
});

export const actions = {
  async getSpecialistAvailability({ commit }, { id }) {
    const service = this.$getAttentionService(AvailabilityService);
    const availability = await service.getSpecialistAvailability({ id });
    commit('storeAvailabilities', availability);
  },
};

export const getters = {
  fromId: _state => id => _state.availabilities[id],
};

export const mutations = {
  storeAvailabilities(_state, availability) {
    Vue.set(_state.availabilities, availability.id, availability);
  },
};
