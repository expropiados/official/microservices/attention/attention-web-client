import { find, propEq } from 'ramda';
import SpecialistAvailableService from '~/services/attention/specialistAvailable-service';
import CompanyService from '~/services/attention/patient-service';
import SpecialistsService from '~/services/backoffice/specialists';

export const state = () => ({
  loading: false,
  specialistsAvailable: [],
  specialists: [],
  error: null,
  message: null,
});

export const actions = {
  async getSpecialists({ commit }, { date, uuidSpecialist }) {
    const service = this.$getAttentionService(SpecialistAvailableService);
    commit('changeLoading', true);
    try {
      const specialists = await service.getAllSpecialistAvailableByDate({ date, uuidSpecialist });
      console.log(specialists.data);
      commit('storeSpecialist', specialists.data);
      commit('changeLoading', false);
    } catch (error) {
      commit('catchError', error);
      commit('changeLoading', false);
      console.error(error);
    }
  },
  async getSpecialistsAvailability({ commit }, { startDate, endDate }) {
    const service = this.$getAttentionService(SpecialistAvailableService);
    commit('changeLoading', true);
    try {
      const specialists = await service.getAllSpecialistAvailableByDateRange({ startDate, endDate });
      commit('storeSpecialist', specialists.data);
      commit('changeLoading', false);
    } catch (error) {
      commit('catchError', error);
      commit('changeLoading', false);
      console.error(error);
    }
  },
  async getAllSpecialists({ commit }) {
    const service = this.$getBackofficeService(SpecialistsService);
    commit('changeLoading', true);
    try {
      const specialists = await service.getSpecialists();
      commit('storeSpecialists', specialists.data);
      commit('changeLoading', false);
    } catch (error) {
      commit('catchError', error);
      commit('changeLoading', false);
      console.error(error);
    }
  },
  async registerAppointment({ commit }, appointment) {
    const service = this.$getAttentionService(CompanyService);
    commit('changeLoading', true);
    try {
      commit('changeLoading', false);
      return await service.registerAppointment(appointment);
    } catch (error) {
      commit('catchError', error);
      console.error(error);
      commit('changeLoading', false);
      return error;
    }
  },
};

const findByUuid = (specialists, uuid) => find(propEq('uuid', uuid))(specialists);

/* example of business logic */
export const getters = {
  firstAppointmentOfToday: _state => _state.specialistsAvailable[0],
  fromUuid: _state => uuid => findByUuid(_state.specialistsAvailable, uuid),
};

export const mutations = {
  storeSpecialist(_state, specialists) {
    _state.specialistsAvailable = specialists;
  },
  storeSpecialists(_state, specialists) {
    _state.specialists = specialists;
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
};
