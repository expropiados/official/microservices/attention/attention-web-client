import CompanyService from "~/services/attention/patient-service";
import AppointmentService from '~/services/attention/appointment-service';
import PatientService from "~/services/backoffice/patient-service";
import Vue from 'vue';

export const state = () => ({
  loading: false,
  error: null,
  message: null,
  appointments: [],
  patientUuid: null,
  patients: [],
});

export const actions = {
  async sendAnswers({ commit }, answers) {
    const service = this.$getAttentionService(CompanyService);
    commit('changeLoading', true);
    try {
      const res = await service.sendAnswers(answers);
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    };
    commit('changeLoading', false);
  },
  async getAppointments({ commit }, { dateIni, dateEnd, patientUuid }) {
    const service = this.$getAttentionService(AppointmentService);
    commit('changeLoading', true);
    try {
      const appointments = await service.getListAppointmentsByPatient({ patientUuid, dateIni, dateEnd });
      commit('storeAppointments', appointments.data);
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    }
    commit('changeLoading', false);
  },
  async getPatients({ commit }, {specialistUuid, name}){
    const service = this.$getAttentionService(PatientService);
    commit('changeLoading', true);
    try {
      const patients = await service.getAllPatients({specialistUuid,name});
      commit('storePatients', patients.data);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  async setPatientUuid({commit}, patientUuid){
    console.log("seteo patient uuid", patientUuid);
    commit('changePatientUuid', patientUuid);
  },
};

export const mutations = {
  storeAppointments(_state, appointments) {
    _state.appointments = appointments;
  },
  storePatients(_state, patients) {
    Vue.set(_state.patients, 'data', patients);
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
  changePatientUuid(_state,patientUuid){
    _state.patientUuid = patientUuid;
  }
};
