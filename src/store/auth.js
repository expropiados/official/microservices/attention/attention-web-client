import Vue from 'vue';
import jwt_decode from 'jwt-decode';
import AuthService from '~/services/security/auth-service';
import { getFilteredRoutes } from '~/helpers/loginroute';

export const state = () => ({
  token: null,
  user: null,
  loading: false,
  error: null,
});

export const actions = {
  async login({ commit, dispatch }, { payload }) {
    const service = this.$getSecurityService(AuthService);
    commit('setLoading', true);
    try {
      const { data } = await service.login({ payload });
      const token = data.accessToken;
      const user = jwt_decode(token);
      dispatch('loginuser', { token, user });
    } catch (error) {
      this.$notifier.showMessage({ content: error, color: "error" })
    };
    commit('setLoading', false);
  },

  async resetPassword({ commit }, { payload }) {
    const service = this.$getSecurityService(AuthService);
    try {
      const { data } = await service.resetPassword({ payload });
      console.log(data);
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    };
  },

  async forgotPassword({ commit }, { payload }) {
    const service = this.$getSecurityService(AuthService);
    try {
      const { data } = await service.forgotPassword({ payload });
      console.log('paso');
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    };
  },

  logout({ commit, dispatch }) {
    this.$cookiz.remove('token', { path: '/', domain: this.$config.appDomain })
    this.$cookiz.remove('user', { path: '/', domain: this.$config.appDomain })
    commit('setUser', { token: null, user: null });
    dispatch('roles/logoutRole', null , { root: true });
    this.$router.push('/');
  },

  loginuser({ commit, dispatch }, { token, user }) {
    console.log(user)
    this.$cookiz.set('token', token, { path: '/', domain: this.$config.appDomain });
    this.$cookiz.set('user', user, { path: '/', domain: this.$config.appDomain });
    commit('setUser', { token, user });
    const routes = getFilteredRoutes(user)
    if (routes.length === 1) {
      const selected = routes[0]
      const options = selected.options;
      const id = selected.id; //0,1,2,3,4
      dispatch('roles/loginRole', { options, role: id }, { root: true });
      window.location = selected.route;
    } else {
      this.$router.push('/roles');
    }

  }
};

export const getters = {
  myUserUuid: _state => _state.user.us_uid,
  myUserName: _state => _state.user.sub,
};

export const mutations = {
  setUser(_state, { token, user }) {
    _state.token = token;
    _state.user = user;
  },
  setLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
};
