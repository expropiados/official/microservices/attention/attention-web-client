import VideoCallService from '~/services/attention/video-call-service';

export const state = () => ({
  agoraName: '',
  loading: false,
  error: null,
  message: null,
});

export const actions = {
  async registerVideoCall({ commit }, videocall) {
    const service = this.$getAttentionService(VideoCallService);
    commit('changeLoading', true);
    try {
      await service.registerVideoCall(videocall);
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    }
    commit('changeLoading', false);
  },
  async getAgoraName({ commit }, videocall) {
    const service = this.$getAttentionService(VideoCallService);
    commit('changeLoading', true);
    try {
      const agoraNameResponse = await service.getAgoraName(videocall);
      commit('changeAgoraName', agoraNameResponse.data);
      commit('changeLoading', false);
      return agoraNameResponse;
    } catch (error) {
      commit('catchError', error);
      commit('changeLoading', false);
      console.error(error);
    }
  },
};

export const mutations = {
  changeAgoraName(_state, agoraName) {
    _state.agoraName = agoraName;
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
};
