import Vue from 'vue';
import TranscriptionService from '@/services/sentiment/transcription-service';

export const state = () => ({
  transcriptionUrl: {},
  transcriptionId: 0,
  dialog: {},
  analysis: {},
  videocallUuid: '',
  videoUrl: '',
  loading: false,
  transcriptionState: 0,
  analysisState: 0,
});

export const actions = {
  async getTranscription({ dispatch }, { sessionId }) {
    console.log("sessionId", sessionId);
    const service = this.$getSentimentService(TranscriptionService);
    try{
      const { data } = await service.getTranscription({ sessionId });
      console.log("data", data);
      console.log("transcriptionId", data.id);
      await dispatch('downloadTranscription', { data });
    } catch (error) {
      console.log("error", {...error});
      await dispatch('setNonStartedSession');
    }
  },
  async getAnalysis({ dispatch }, { transcriptionId, videocallUuid }) {
    console.log("transcriptionId", transcriptionId);
    console.log("videocallUuid", videocallUuid);
    const service = this.$getSentimentService(TranscriptionService);
    const { data } = await service.getAnalysis({ transcriptionId, videocallUuid });
    console.log("get analysis : data" , data);
    await dispatch('downloadAnalysis', { data });
  },
  async downloadTranscription({ commit }, { data }) {
    const { transcriptionUrl, videocallUuid, jobState } = data
    commit('storeTranscriptionId', data.id);
    if (jobState == 0) {
      commit('storeTranscriptionState', 0);
      commit('storeAnalysisState', 0);
      return;
    }
    try {
      const fetchResponse = await fetch(transcriptionUrl);
      console.log("fetchR", fetchResponse);
      //const { compmlete_text } = await fetchResponse.json();
      //commit('storeDialog', compmlete_text);
      const { dialogue, video_url } = await fetchResponse.json();
      commit('storeDialog', dialogue);
      commit('storeVideocallUuid', videocallUuid);
      commit('storeVideoUrl', video_url);
      commit('storeTranscriptionState', jobState);
      commit('storeEstadoSesion', 1);
    } catch (err) {
      this.$notifier.showMessage({ content: 'Error en cargar datos', color: "error" })
    }
  },
  async downloadAnalysis({ commit }, { data }) {
    console.log("datadata", data);
    const { textAnalysisUrl, jobState } = data
    if (jobState == 0) {
      commit('storeTranscriptionState', 0);
      commit('storeAnalysisState', 0);
      return;
    }
    try {
      const fetchResponse = await fetch(textAnalysisUrl);
      const analysis = await fetchResponse.json();
      commit('storeAnalysis', analysis);
      commit('storeAnalysisState', jobState);
      commit('storeEstadoSesion', 1);
    } catch (err) {
      this.$notifier.showMessage({ content: 'Error en cargar datos', color: "error" })
    }
  },
  async setNonStartedSession( {commit} ) {
    commit('storeAnalysisState', -1);
    commit('storeTranscriptionState', -1);
    commit('storeEstadoSesion', 0);
  }
};

export const getters = {
};

export const mutations = {
  storeDialog(_state, dialog) {
    console.log("coomplete", dialog);
    _state.dialog = dialog;
  },
  storeAnalysis(_state, analysis) {
    _state.analysis = analysis;
  },
  storeVideocallUuid(_state, videocallUuid) {
    console.log("videocallUuid", videocallUuid);
    _state.videocallUuid = videocallUuid;
  },
  storeVideoUrl(_state, videoUrl) {
    console.log("videoURL", videoUrl);
    _state.videoUrl = videoUrl;
  },
  storeTranscriptionId(_state, transcriptionId) {
    console.log("transcriptionId", transcriptionId);
    _state.transcriptionId = transcriptionId;
  },
  storeTranscriptionState(_state, jobState) {
    console.log("transcriptionState", jobState);
    _state.transcriptionState = jobState;
  },
  storeAnalysisState(_state, jobState) {
    console.log("analysisState", jobState);
    _state.analysisState = jobState;
  },
  storeEstadoSesion(_state, estadoSesion) {
    _state.estadoSesion = estadoSesion;
  }
};
