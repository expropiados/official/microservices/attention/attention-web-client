import Vue from 'vue';
import AppointmentService from '@/services/attention/appointment-service';

export const state = () => ({
  appointment: {},
  appointments: {},
  error: null,
  reasons: [],
  loading: false,
});

export const actions = {
  async saveResults({ commit }, { sessionId, results }) {
    const service = this.$getAttentionService(AppointmentService);
    try {
      const response = await service.saveResults({ sessionId, results });
      console.log(response);
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    };
  },

  async getAppointment({ commit }, { id }) {
    const service = this.$getAttentionService(AppointmentService);
    const response = await service.getAppointment({ id });
    const appointment = response.data.appointment;
    appointment.appointmentDetail = {};
    appointment.appointmentDetail.idAppointmentDetail = response.data.idAppointmentDetail;
    appointment.appointmentDetail.comment = response.data.comment;
    appointment.resources = response.data.resources;
    commit('storeAppointment', appointment);
    commit('setAppointment', appointment);
  },
  async getReasonPreAppointment({ commit }) {
    const service = this.$getAttentionService(AppointmentService);
    commit('changeLoading', true);
    try {
      const reasons = await service.getReasonsPreAppointment();
      console.log('imprime las razones', reasons.data);
      commit('storeReasons', reasons.data);
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    }
    commit('changeLoading', false);
  },
  async registerPrePoll({ commit }, { idAppointment, payload }) {
    const service = this.$getAttentionService(AppointmentService);
    commit('changeLoading', true);
    try {
      const response = await service.registerPrePolls({ idAppointment, payload });
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    }
    commit('changeLoading', false);
  },
};

export const getters = {
  fromId: _state => id => _state.appointments[id],
};

export const mutations = {
  storeAppointment(_state, appointment) {
    Vue.set(_state.appointments, appointment.id, appointment);
  },
  setAppointment(_state, appointment) {
    _state.appointment = appointment;
  },
  catchError(_state, error) {
    _state.error = error;
  },
  storeReasons(_state, reasons) {
    _state.reasons = reasons;
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  }
};
