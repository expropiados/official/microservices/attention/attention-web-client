import { isNil } from 'ramda';
import rolesinfo from "@/assets/js/rolesinfo.js"

export const actions = {
  nuxtServerInit({ commit }) {
    const token = this.$cookiz.get('token');
    const user = this.$cookiz.get('user');
    const role = this.$cookiz.get('role');
    if (token && user) {
      commit('auth/setUser', { token, user });
      if (!isNil(role) && role >= 0) {
        commit('roles/setRole', role )
        const rolestate = rolesinfo.find(el => el.id === role)
        if (rolestate) commit('roles/setOptions', rolestate.options )
     };
    }
  },
};
