export const state = () => ({
  options: null,
  role: null,
});

export const actions = {
  loginRole({ commit }, { options, role }) {
    commit('setOptions', options);
    commit('setRole', role);
    this.$cookiz.set('role', role, { path: '/', domain: this.$config.appDomain });
  },
  logoutRole({ commit }) {
    commit('setOptions', null);
    commit('setRole', null);
    this.$cookiz.remove('role', { path: '/', domain: this.$config.appDomain });
  },
};

export const mutations = {
  setOptions(_state, options) {
    _state.options = options;
  },
  setRole(_state, role) {
    _state.role = role;
  },
};
