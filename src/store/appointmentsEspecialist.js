import Vue from 'vue';
import CompanyService from '@/services/backoffice/company-service';

export const state = () => ({
  company: {},
  companies: {},
});

export const actions = {
  async getCompany({ commit }, { id }) {
    const service = this.$getBackofficeService(CompanyService);
    const company = await service.getCompany({ id });
    commit('storeCompany', company);
  },
};

export const getters = {
  fromId: _state => id => _state.companies[id],
  categoryName: _state => _state.company.name,
};

export const mutations = {
  storeCompany(_state, company) {
    Vue.set(_state.companies, company.id, company);
  },
};
