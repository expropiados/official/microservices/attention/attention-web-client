import Vue from 'vue';
import AvailabilityService from '@/services/attention/calendar-service';

export const state = () => ({
  loading: false,
  availability: [],
  error: null,
  message: null,
});

export const actions = {
  async registerAvailability({ commit }, { specialistUuid, payload }) {
    const service = this.$getAttentionService(AvailabilityService);
    commit('changeLoading', true);
    try {
      const response = await service.registerAvailability({ specialistUuid, payload });
      this.$notifier.showMessage({ content: 'Disponibilidad modificada' })
    } catch (error) {
      commit('catchError', error);
      this.$notifier.showMessage({ content: error, color: "error"})
    };
    commit('changeLoading', false);
  },

  async getAvailability({ commit }, { specialistUuid, dateBegin, dateEnd }) {
    const service = this.$getAttentionService(AvailabilityService);
    commit('changeLoading', true);
    try {
      const { data } = await service.getAvailability({ specialistUuid, dateBegin, dateEnd });
      commit('storeAvailability', data);
    } catch ({ response }) {
      commit('catchError', response);
      console.log(response);
    };
    commit('changeLoading', false);
  },
};
/* example of business logic */
export const getters = {
  availability: _state => _state.availability,
};

export const mutations = {
  storeAvailability(_state, availability) {
    _state.availability = availability;
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
};
