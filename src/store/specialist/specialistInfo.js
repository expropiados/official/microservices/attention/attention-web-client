import SpecialistsService from '@/services/backoffice/specialists';

export const state = () => ({
  specialistExtraInfo: [],
  specialistInfo: [],
  loading: false,
  error: null,
});

export const actions = {
  async getSpecialistInfoExtra({ commit }, { specialistUuid }) {
    const service = this.$getBackofficeService(SpecialistsService);
    commit('changeLoading', true);
    try {
      const specialistInfo = await service.getInfoExtraSpecialist({ specialistUuid });
      commit('storeSpecialistInfoExtra', specialistInfo.data);
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    }
    commit('changeLoading', false);
  },
  async getSpecialistInfo({ commit }, { specialistUuid }) {
    const service = this.$getBackofficeService(SpecialistsService);
    commit('changeLoading', true);
    try {
      const specialistInfo = await service.getInfoSpecialist({ specialistUuid });
      commit('storeSpecialistInfo', specialistInfo.data);
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    }
    commit('changeLoading', false);
  },
};

export const mutations = {
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
  storeSpecialistInfoExtra(_state, specialistInfo) {
    _state.specialistExtraInfo = specialistInfo;
  },
  storeSpecialistInfo(_state, specialistInfo) {
    _state.specialistInfo = specialistInfo;
  },
};
