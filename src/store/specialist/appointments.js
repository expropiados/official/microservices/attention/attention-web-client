import AppointmentService from '@/services/attention/appointment-service';

export const state = () => ({
  loading: false,
  appointments: [],
  detailAppointment: {},
  appointmentsPatient: {},
  error: null,
  message: null,
});

export const actions = {
  async registerLinkInDetailAppointment({ commit }, { idAppointment, payload }) {
    const service = this.$getAttentionService(AppointmentService);
    commit('changeLoading', true);
    try {
      const response = await service.registerLinkInDetailAppointment({ idAppointment, payload });
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    }
    commit('changeLoading', false);
  },
  async registerDocumentInDetailAppointment({ commit }, { idAppointment, payload }) {
    const service = this.$getAttentionService(AppointmentService);
    commit('changeLoading', true);
    try {
      const responde = await service.registerDocumentInDetailAppointment({ idAppointment, payload });
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    }
    commit('changeLoading', false);
  },
  async getAppointmentDetail({ commit }, { idAppointment }) {
    const service = this.$getAttentionService(AppointmentService);
    commit('changeLoading', true);
    try {
      const detailAppointment = await service.getAppointmentDetail({ idAppointment });
      commit('storeDetailAppointment', detailAppointment.data);
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    }
    commit('changeLoading', false);
  },
  async getAppointments({ commit }, { dateIni, dateEnd, specialistUuid }) {
    const service = this.$getAttentionService(AppointmentService);
    commit('changeLoading', true);
    try {
      const appointments = await service.getAllAppointmentsByDate({ specialistUuid, dateIni, dateEnd });
      commit('storeAppointments', appointments.data);
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    }
    commit('changeLoading', false);
  },
  async getAppointmentsPatient({ commit }, { specialistUuid, patientUuid }) {
    const service = this.$getAttentionService(AppointmentService);
    commit('changeLoading', true);
    try {
      const appointments = await service.getAllAppointmentsBySpecialistPatient({ specialistUuid, patientUuid });
      console.log(appointments.data.user.dni);
      commit('storeAppointmentsPatient', appointments.data);
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    }
    commit('changeLoading', false);
  },
  async cancelAppointments({ commit }, { idAppointment }) {
    const service = this.$getAttentionService(AppointmentService);
    commit('changeLoading', true);
    try {
      const responde = await service.cancelAppointment({ idAppointment });
    } catch (error) {
      commit('catchError', error);
      console.error(error);
    }
    commit('changeLoading', false);
  },
};
/* example of business logic */
export const getters = {
  firstAppointmentOfToday: _state => _state.appointments[0],
  getVideocallUuid: _state => _state.detailAppointment.appointment.videoCallUuid,
};

export const mutations = {
  storeDetailAppointment(_state, detailAppointment) {
    _state.detailAppointment = detailAppointment;
  },
  storeAppointments(_state, appointments) {
    _state.appointments = appointments;
  },
  storeAppointmentsPatient(_state, appointments) {
    _state.appointmentsPatient = appointments;
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
};
