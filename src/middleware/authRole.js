export default function ({ redirect, route, store }) {
  if (store.state.roles.role === null) {
    return redirect('/roles')
  }
}
