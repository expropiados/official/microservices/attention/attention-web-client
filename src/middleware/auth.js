export default function ({ redirect, route, store }) {
  if (!store.state.auth.token) {
    return redirect('/')
  }
}
